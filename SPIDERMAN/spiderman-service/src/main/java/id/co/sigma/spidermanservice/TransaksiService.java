package id.co.sigma.spidermanservice;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.spidermandao.feign.ThanosIntervace;
import id.co.sigma.spidermandao.pojo.response.TransaksiResponse;

@Service("transaksiservice")
public class TransaksiService {
	
	@Autowired
	ThanosIntervace thanosinterface;
	
	public TransaksiResponse cekSaldo(String rekening) {
		return thanosinterface.cekSaldo(rekening);
	}
	
	public TransaksiResponse setorTunai(String rekening, BigDecimal jumlah) {
		return thanosinterface.setorTunai(rekening, jumlah);
	}
	
	public TransaksiResponse tarikTunai(String rekening, BigDecimal jumlah) {
		return thanosinterface.tarikTunai(rekening, jumlah);
	}
}
