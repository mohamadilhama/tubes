package id.co.sigma.spidermanservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.spidermandao.feign.ThanosIntervace;

@Service
public class LoginService {
	
	@Autowired
	ThanosIntervace thanosintervace;
	
	public String userCheck (String username, String password) {
		return thanosintervace.login(username, password);
	}
	

}
