package id.co.sigma.spidermancontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import id.co.sigma.spidermandao.pojo.request.LoginRequest;
import id.co.sigma.spidermandao.pojo.response.BaseResponse;
import id.co.sigma.spidermanservice.LoginService;

@Controller("logincontroller")
public class LoginController {
	
	@Autowired
	private LoginService loginservice;
	
	@RequestMapping(value="/webservice/login**", method = {RequestMethod.POST})
	public @ResponseBody
	BaseResponse login (@RequestBody LoginRequest request) {
		BaseResponse response = new BaseResponse();
		String result =loginservice.userCheck(request.getUsername(), request.getPassword());
		if ("SUCCESS".equals(result)) {
			response.setResponseCode("00");
			response.setResponseMessage("LOGIN"+result);
		}else {
			response.setResponseCode("01");
			response.setResponseMessage(result);
		}
		return response;
		
		}
	

	
	

}
