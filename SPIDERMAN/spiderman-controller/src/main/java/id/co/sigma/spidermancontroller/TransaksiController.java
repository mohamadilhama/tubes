package id.co.sigma.spidermancontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import id.co.sigma.spidermandao.pojo.request.CekSaldo;
import id.co.sigma.spidermandao.pojo.request.TransaksiRequest;
import id.co.sigma.spidermandao.pojo.response.TransaksiResponse;
import id.co.sigma.spidermanservice.TransaksiService;

@Controller("transaksicontroller")
public class TransaksiController {
	Logger logger = LoggerFactory.getLogger(TransaksiController.class);
		@Autowired
		private TransaksiService transaksiservice;
		
		@RequestMapping(value="/webservice/cekSaldo**", method=RequestMethod.POST)
		public @ResponseBody TransaksiResponse cekSaldo(@RequestBody CekSaldo request) {
			TransaksiResponse response = new TransaksiResponse();
			TransaksiResponse nasabahdata = transaksiservice.cekSaldo(request.getRekening());
			response.setNama(nasabahdata.getNama());
			response.setRekening(nasabahdata.getRekening());
			logger.info("Saldo="+nasabahdata.getSaldo());
			response.setSaldo(nasabahdata.getSaldo());
			return response;
		}
		
		@RequestMapping(value="/webservice/tarikTunai**", method=RequestMethod.POST)
		public @ResponseBody TransaksiResponse tarikTunai(@RequestBody TransaksiRequest request) {
			return transaksiservice.tarikTunai(request.getRekening(), request.getJumlah());
		}
		
		@RequestMapping(value="/webservice/setorTunai**", method=RequestMethod.POST)
		public @ResponseBody TransaksiResponse setorTunai(@RequestBody TransaksiRequest request) {
			return transaksiservice.setorTunai(request.getRekening(), request.getJumlah());
		}
		
		
		
}
