package id.co.sigma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("id.co.sigma")
@Configuration
@EnableFeignClients
@EnableDiscoveryClient
public class SpidermanControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpidermanControllerApplication.class, args);
	}
}
