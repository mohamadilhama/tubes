package id.co.sigma.spidermandao.feign;

import java.math.BigDecimal;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import id.co.sigma.spidermandao.pojo.response.TransaksiResponse;

@FeignClient("thanos")
public interface ThanosIntervace {
	
	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliSecond", value="300000")
	@RequestMapping(value = "/app/internal/userCheck", method = {RequestMethod.POST})
	String login (@RequestParam("username") String username, @RequestParam("password") String password);
	
	@HystrixCommand
	@HystrixProperty(name="hysterix.command.default.execution.isolation.thread.timeoutInMilliSecond", value="300000")
	@RequestMapping(value="/app/internal/cekSaldo", method= {RequestMethod.POST})
	TransaksiResponse cekSaldo(@RequestParam("rekening") String rekening);
	
	@HystrixCommand
	@HystrixProperty(name="hysterix.command.default.execution.isolation.thread.timeoutInMilliSecond", value="300000")
	@RequestMapping(value="/app/internal/setorTunai", method= {RequestMethod.POST})
	TransaksiResponse setorTunai(@RequestParam("rekening") String rekening, @RequestParam("jumlah") BigDecimal jumlah);
	
	@HystrixCommand
	@HystrixProperty(name="hysterix.command.default.execution.isolation.thread.timeoutInMilliSecond", value="300000")
	@RequestMapping(value="/app/internal/tarikTunai", method= {RequestMethod.POST})
	TransaksiResponse tarikTunai(@RequestParam("rekening") String rekening, @RequestParam("jumlah") BigDecimal jumlah);
}
