package id.co.sigma.spidermandao.pojo.request;

import java.math.BigDecimal;

public class TransaksiRequest {

	private String rekening;
	private BigDecimal jumlah;
	
	public String getRekening() {
		return rekening;
	}
	public void setRekening(String rekening) {
		this.rekening = rekening;
	}
	public BigDecimal getJumlah() {
		return jumlah;
	}
	public void setJumlah(BigDecimal jumlah) {
		this.jumlah = jumlah;
	}
	@Override
	public String toString() {
		return "TransaksiRequest [rekening=" + rekening + ", jumlah=" + jumlah + "]";
	}

	
	
	

}
