package id.co.sigma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@SpringBootApplication
@EnableAutoConfiguration
@ConfigurationProperties
@ComponentScan
@Configuration
@EnableDiscoveryClient
@EnableCaching
public class ThanosControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThanosControllerApplication.class, args);
	}
}
