package id.co.sigma.thanoscontroller.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.thanosdao.pojo.request.EchoRequest;
import id.co.sigma.thanosdao.pojo.response.BaseResponse;



@RestController
public class EchoController {

	public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";

	@RequestMapping(value = TERMINAL_ECHO_PATH, method = RequestMethod.POST)
	public @ResponseBody BaseResponse echo(@RequestBody EchoRequest request)  {
		BaseResponse response = new BaseResponse();
		if (request.getRequestCode().equals("echo")) {
			response.setResponseCode("00");
			response.setResponseMessage("SUCCESS");
		} else {
			response.setResponseCode("01");
			response.setResponseMessage("FAILED");
		}
		return response;

	}
}
