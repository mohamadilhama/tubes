package id.co.sigma.thanoscontroller.configuration;

import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@Configuration
public class HazelcastConfiguaration {
	
	@Bean
	HazelcastInstance hazelcaseInstance() {
		ClientConfig config = new ClientConfig();
		config.getGroupConfig().setName("sigma").setPassword("sigma-pass");
		config.getNetworkConfig().addAddress("localhost");
		HazelcastInstance instance = HazelcastClient.newHazelcastClient(config);
		return instance;
	}
	@Bean
	CacheManager cacheManager() {
		return new HazelcastCacheManager(hazelcaseInstance());
	}

}
