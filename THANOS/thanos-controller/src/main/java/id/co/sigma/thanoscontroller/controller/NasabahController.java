package id.co.sigma.thanoscontroller.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.thanosdao.entity.Nasabah;
import id.co.sigma.thanosdao.pojo.request.NasabahRequest;
import id.co.sigma.thanosdao.pojo.response.AddUserResponse;
import id.co.sigma.thanosdao.pojo.response.TransaksiResponse;
import id.co.sigma.thanosservice.NasabahService;

@RestController
public class NasabahController {
	Logger logger = LoggerFactory.getLogger(NasabahController.class);
	@Autowired
	private NasabahService nasabahservice;

	@RequestMapping(value = "/internal/setorTunai**", method = { RequestMethod.POST })
	public TransaksiResponse setorTunai(String rekening, BigDecimal jumlah) {
		TransaksiResponse response = new TransaksiResponse();
		Nasabah nasabah = nasabahservice.getNasabahByRekening(rekening);
		nasabah.setSaldo(nasabah.getSaldo().add(jumlah));
		nasabahservice.updateNasabah(nasabah);
		response.setNama(nasabahservice.getNasabahByRekening(rekening).getNama());
		response.setRekening(nasabahservice.getNasabahByRekening(rekening).getRekening());
		response.setSaldo(nasabahservice.getNasabahByRekening(rekening).getSaldo());
		return response;
	}

	@RequestMapping(value = "/internal/tarikTunai**", method = { RequestMethod.POST })
	public TransaksiResponse tarikTunai(String rekening, BigDecimal jumlah) {
		TransaksiResponse response = new TransaksiResponse();
		Nasabah nasabah = nasabahservice.getNasabahByRekening(rekening);
		nasabah.setSaldo(nasabah.getSaldo().subtract(jumlah));
		nasabahservice.updateNasabah(nasabah);
		response.setNama(nasabahservice.getNasabahByRekening(rekening).getNama());
		response.setRekening(nasabahservice.getNasabahByRekening(rekening).getRekening());
		response.setSaldo(nasabahservice.getNasabahByRekening(rekening).getSaldo());
		return response;
	}

	@RequestMapping(value = "/internal/cekSaldo**", method = { RequestMethod.POST })
	public @ResponseBody TransaksiResponse cekSaldo(@RequestParam String rekening) {
		TransaksiResponse response = new TransaksiResponse();
		Nasabah nasabah = nasabahservice.getNasabahByRekening(rekening);
		response.setRekening(nasabah.getRekening());
		response.setNama(nasabah.getNama());
		logger.info("Saldo="+nasabah.getSaldo());
		response.setSaldo(nasabah.getSaldo());
		return response;
	}

	@RequestMapping(value = "/webservice/addNasabah**", method = { RequestMethod.POST })
	public @ResponseBody AddUserResponse insertNasabah(@RequestBody NasabahRequest request) {
		AddUserResponse response = new AddUserResponse();
		String nasabahId = "";
		Nasabah nasabah = new Nasabah();
		nasabah.setNama(request.getNama());
		nasabah.setRekening(request.getRekening());
		nasabah.setSaldo(request.getSaldo());
		nasabahservice.addNasabah(nasabah);
		nasabahId = nasabah.getId().toString();
		response.setUserId(nasabahId);
		response.setResponseCode("00");
		response.setResponseMessage("SUCCESS");
		return response;
	}

}