package id.co.sigma.thanoscontroller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.thanosdao.entity.Users;
import id.co.sigma.thanosdao.pojo.request.AddUserRequest;
import id.co.sigma.thanosdao.pojo.request.UpdateUserRequest;
import id.co.sigma.thanosdao.pojo.response.AddUserResponse;
import id.co.sigma.thanosservice.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userservice;
	
	
	@RequestMapping (value = "/internal/userCheck**", method = {RequestMethod.POST})
	public String login (@RequestParam String username, @RequestParam String password) {
		Users user = userservice.findByUsernameAndPassword(username, password);
		if (user != null) {
			return "SUCCESS";
		}else {
			return "FAILED";
		}
		
	}
	@RequestMapping (value = "/webservice/addUser**", method= {RequestMethod.POST})
	public @ResponseBody
	 AddUserResponse insertUser(@RequestBody AddUserRequest request) {
        AddUserResponse response = new AddUserResponse();
        String userId = "";
        Users user = new Users();
        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());
        user.setRole(request.getRole());
        userservice.insertUser(user);
        userId = user.getUserId().toString();
        response.setUserId(userId);
        response.setResponseCode("00");
        response.setResponseMessage("SUCCESS");
		return response;
        
    }
	 @RequestMapping(value = "/webservice/updateUser**", method = {RequestMethod.POST})
	    public @ResponseBody
	    AddUserResponse updateUser(@RequestBody UpdateUserRequest request) {
	        AddUserResponse response = new AddUserResponse();
	        String userId = "";
	        Users user = userservice.findByUsername(request.getUsername());
	        user.setUsername(request.getUsername());
	        user.setRole(request.getRole());
	        userservice.updatetUser(user);
	        userId = user.getUserId().toString();
	        response.setUserId(userId);
	        response.setResponseCode("00");
	        response.setResponseMessage("SUCCESS");
	        return response;
	    }
	
	
	
	

}
