package id.co.sigma.thanosservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.sigma.thanosdao.entity.Users;
import id.co.sigma.thanosdao.pojo.repository.UserDao;

@Service("userservice")
public class UserService {

	@Autowired
	private UserDao userdao;
	
	
	
	@Cacheable(value = "thanos.user.findByUsernameAndPasswod", unless = "#result==null")
	public Users findByUsernameAndPassword(String username, String password) {
		return userdao.findByUsernameAndPassword(username, password);
		
	}
	@Caching(evict= {
			@CacheEvict(value = "thanos.user.findByUsernameAndPassword", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "thanos.user.findByUsername", allEntries = true, beforeInvocation = true)})
	public void insertUser(Users user) {
		userdao.save(user);
		}
	
	@Caching(evict= {
			@CacheEvict(value = "thanos.user.findByUsernameAndPassword", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "thanos.user.findByUsername", allEntries = true, beforeInvocation = true)})
	public void updatetUser(Users user) {
		userdao.save(user);
		}
	@Cacheable(value ="thanos.user.findByUsername", unless = "#result ==null")
	public Users findByUsername( String username) {
		return userdao.findByUsername(username);
	}
	

}
