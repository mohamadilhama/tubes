package id.co.sigma.thanosservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.thanosdao.entity.Nasabah;
import id.co.sigma.thanosdao.pojo.repository.NasabahDao;

@Service("nasabahService")
public class NasabahService {
		@Autowired
		private NasabahDao nasabahdao;
		
		public Nasabah getNasabahByRekening(String rekening) {
			return this.nasabahdao.findByRekening(rekening);
		}
		
		public void addNasabah(Nasabah nasabah) {
			this.nasabahdao.save(nasabah);
		}
		
		public void updateNasabah(Nasabah nasabah) {
			this.nasabahdao.save(nasabah);
		}
	
}
