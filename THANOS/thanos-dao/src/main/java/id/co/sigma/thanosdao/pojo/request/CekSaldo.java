package id.co.sigma.thanosdao.pojo.request;

public class CekSaldo {
	
	private String rekening;

	public String getRekening() {
		return rekening;
	}

	public void setRekening(String rekening) {
		this.rekening = rekening;
	}

	@Override
	public String toString() {
		return "CekSaldo [rekening=" + rekening + "]";
	}
	
	

}
