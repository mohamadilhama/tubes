package id.co.sigma.thanosdao.pojo.request;

import java.math.BigDecimal;

public class TransaksiRequest {

	private String rekening;
	private BigDecimal saldo;
	
	
	public String getRekening() {
		return rekening;
	}
	public void setRekening(String rekening) {
		this.rekening = rekening;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public String toString() {
		return "TransaksiRequest [rekening=" + rekening + ", saldo=" + saldo + "]";
	}

	
	

}
