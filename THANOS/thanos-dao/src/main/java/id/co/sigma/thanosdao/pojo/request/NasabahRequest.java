package id.co.sigma.thanosdao.pojo.request;

import java.math.BigDecimal;

public class NasabahRequest {
	private String nama;
	private String rekening;
	private BigDecimal saldo;
	
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getRekening() {
		return rekening;
	}
	public void setRekening(String rekening) {
		this.rekening = rekening;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	@Override
	public String toString() {
		return "NasabahRequest [nama=" + nama + ", rekening=" + rekening + ", saldo=" + saldo + "]";
	}
	

}
