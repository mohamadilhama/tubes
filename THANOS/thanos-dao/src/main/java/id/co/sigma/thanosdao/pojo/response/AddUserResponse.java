package id.co.sigma.thanosdao.pojo.response;

public class AddUserResponse extends BaseResponse{
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "AddUserResponse [userId=" + userId + "]";
	}


		
	

}
