package id.co.sigma.thanosdao.pojo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.thanosdao.entity.Users;

public interface UserDao extends JpaRepository<Users, Long> {
	Users findByUsernameAndPassword(String username, String password);
	Users findByUsername(String username);

}
