package id.co.sigma.thanosdao.pojo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.thanosdao.entity.Nasabah;

public interface NasabahDao extends JpaRepository<Nasabah, Long>{
	Nasabah findByRekening(String rekening);
	

}
