package id.co.sigma.thanosdao.pojo.request;

public class EchoRequest {
	private String requestCode;

	public String getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

}
